package com.library.tests;


/*Napisz program do obsługi biblioteki. Ma posiadać następujące funkcjonalności:

        - Program działa dopóki użytkownik nie wpisze "Exit", każda funkcjonalność oferuje podmenu i
        możliwość przemieszczania się między funkcjonalnościami
        - Katalog książek (wystarczy kilka) do wypożyczenia (wszystkie możliwe książki powinny być zapisane w pliku)
        - Książka musi mieć minimum: tytuł, rok wydania, autora oraz swoje ID (proponuję użyć tutaj ISBN)
        - Dane o wypożyczonych książkach trzymamy w pamięci (wybierz odpowiednią strukturę danych), stwórz klasę, która będzie reprezentować repozytorium książek
        - Ta klasa jako jedyna ma możliwość wykonywania operacji typu weź książkę, wypożycz książkę itp.
        - Przez interakcję przez konsolę, użytkownik może wypożyczyć egzemplarz danej książki, jeśli jest on dostępny. Jeśli nie, powinien się o tym dowiedzieć
        - Użytkownik może wyświetlić listę wszystkich dostępnych książek
        //- Użytkownik może wyszukać książki po wszystkich atrybutach (fraza tytułu, rok wydania, autor, ID)
        //- Użytkownik może dodać nową książkę do zbioru (donacja)
        - Biblioteka nie przyjmuje duplikatów, jeśli książka o zadanym ISBN już istnieje w zbiorze, nie przyjmuje donacji
        - Spróbuj zrobić kod obiektowo, tj. przy użyciu jak najmniejszej ilości statycznych metod i pól.
        Wyjątkiem jest metoda main lub jeśli używasz słówka static świadomie (np. deklarując jakąś stałą w klasie)*/

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.library.solution.consoleParts.ConsoleApp;
import com.library.solution.libraryElements.Library;
import com.library.solution.libraryElements.book.Book;
import com.library.solution.libraryElements.book.ISBN;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Year;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ConsoleApp cApp = new ConsoleApp();
       cApp.run("National Library");
        Book book =new Book(Year.of(1234),"","",new ISBN("1231231231234"));
        Book book2 =new Book(Year.of(1234),"","",new ISBN("1231231231234"));

        List<Book> books = new ArrayList<Book>();

        books.add(book);
        books.add(book2);

        ObjectMapper om = new ObjectMapper();
        om.findAndRegisterModules();
        String value = "";

        try {
           value = om.writeValueAsString(books);
            System.out.println(value);
           // System.out.println(value);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //om.writerWithView(List.class);

        ObjectMapper omr = new ObjectMapper();
        ArrayList<Book> bk = null;
        try {
            bk = omr.readValue(value, new TypeReference<ArrayList<Book>>() {
                @Override
                public Type getType() {
                    return super.getType();
                }
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        for (Book b: bk
             ) {
            System.out.println(b);

        }
        //List <Object> b = null;
       /* try {
            //b =  om.readValue(value,List.class);
            Book bok = om.readValue(value.trim(),Book.class) ;
            System.out.println(bok.getIsbn());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }*/
        //System.out.println( b.get(1));


    }
}