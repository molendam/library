package com.library.solution.comunnication;

public class Response {

    private String message;
    private boolean status;

    public Response(String message, boolean status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public boolean getStatus() {
        return status;
    }

}
