package com.library.solution.libraryElements;

import com.library.solution.comunnication.Response;
import com.library.solution.dataManager.BookDataManager;
import com.library.solution.enums.DataType;
import com.library.solution.enums.SearchBy;
import com.library.solution.libraryElements.book.Book;
import com.library.solution.libraryElements.book.ISBN;

import java.io.IOException;
import java.time.Year;
import java.util.*;

public class BookRepository {
    private final String ALREADY_EXIST = "Book already exist in repo!";
    private final String ADDED_CORRECTLY = "Book added correctly!";
    private final String REMOVED_CORRECTLY = "Book removed correctly!";
    private final String DOES_NOT_EXIST = "Book does not exist!";

    public Map<ISBN, Book> getBooksMap() {
        return booksMap;
    }

    private Map<ISBN, Book> booksMap = new HashMap<>();
    private BookDataManager bookDataManager = new BookDataManager();

    public Response addBook(String title, String author, ISBN isbn, Year year) {
        Book book = new Book(year, title, author, isbn);
        if (booksMap.containsKey(isbn)) {
            return new Response(ALREADY_EXIST, false);
        }
        booksMap.put(isbn, book);
        return new Response(ADDED_CORRECTLY, true);
    }

    public Response addBook(Book book) {
        if (booksMap.containsKey(book.getIsbn())) {
            return new Response(ALREADY_EXIST, false);
        }
        booksMap.put(book.getIsbn(), book);
        return new Response(ADDED_CORRECTLY, true);
    }

    public Optional<Book> findBookByISBN(ISBN isbn) {
        return Optional.ofNullable(booksMap.get(isbn));
        /*if (booksMap.containsKey(isbn)) {
            return Optional.of(booksMap.get(isbn));
        }
        return Optional.empty();*/
    }


    public List<Book> findBooks(String userInput, SearchBy searchingType) {
        List< Book> booksFound = new ArrayList<>();


        switch (searchingType) {
            case ISBN:
                ISBN isbn = new ISBN(userInput);
                Optional o = findBookByISBN(isbn);
                if (o.isPresent()) {
                    Book b = (Book) o.get();
                    booksFound.add(b);
                }
                break;
            case TITLE:
                for (Book b : booksMap.values()) {
                    String title = b.getTitle();
                    if (title.toLowerCase().contains(userInput.toLowerCase())) {
                        booksFound.add( b);
                    }
                }
                break;
            case AUTHOR:
                for (Book b : booksMap.values()) {
                    String author = b.getAuthor();
                    if (author.toLowerCase().contains(userInput.toLowerCase())) {
                        booksFound.add( b);
                    }
                }
                break;
            case ANY_INFORMATION:
                for (Book b : booksMap.values()) {
                    String allBookFields = b.getTitle() + b.getAuthor() + b.getPublicationYear();
                    if (allBookFields.toLowerCase().contains(userInput.toLowerCase())) {
                        booksFound.add( b);
                    }
                }
                break;
        }
        return booksFound;
    }

    public Response removeBook(ISBN isbn) {
        if (!booksMap.containsKey(isbn)) {
            return new Response(DOES_NOT_EXIST, false);
        }
        booksMap.remove(isbn);
        return new Response(REMOVED_CORRECTLY, true);
    }


    public void loadData(DataType loadDataType) throws IOException, ClassNotFoundException {
        List<Book> bookList = bookDataManager.loadData(loadDataType);
        for (Book b: bookList
             ) {
            booksMap.put(b.getIsbn(),b);
        }
    }

    public void saveData(DataType saveDataType) throws IOException {

        bookDataManager.saveData(saveDataType, new ArrayList<>(getBooksMap().values()));
    }

}








