package com.library.solution.libraryElements;

import com.library.solution.libraryElements.reservations.ReservationRepository;

public class Library {
    public Library() {
/*
        ISBN isbn1 = new ISBN("1234123412341");
        ISBN isbn2 = new ISBN("1234123412341");
        ISBN isbn3 = new ISBN("2342342342344");
        ISBN isbn4 = new ISBN("3453453453455");
        ISBN isbn5 = new ISBN("5675675675677");
        ISBN isbn6 = new ISBN("6786786786788");
        ISBN isbn7 = new ISBN("7897897897899");
        ISBN isbn8 = new ISBN("0120120120120");
        Book b1 = new Book(Year.of(1900), "Krzyżacy", "Henryk Sienkiewicz", isbn1);
        Book b2 = new Book(Year.of(1901), "Boska komedia", "Dante Alighieri", isbn2);
        Book b3 = new Book(Year.of(1902), "Makbet", "William Szekspir", isbn3);
        Book b4 = new Book(Year.of(1905), "Romeo i Julia", "William Szekspir", isbn4);
        Book b5 = new Book(Year.of(1908), "Antygona", "Sofokles", isbn5);
        Book b6 = new Book(Year.of(1976), "Wesele", "Stanisław Wyspiański", isbn6);
        Book b7 = new Book(Year.of(1900), "Przedwiośnie", "Stefan Żeromski", isbn7);
        bookRepository.addBook(b1);
        bookRepository.addBook(b1);
        bookRepository.addBook(b2);
        bookRepository.addBook(b3);
        bookRepository.addBook(b4);
        bookRepository.addBook(b5);
        bookRepository.addBook(b6);
        bookRepository.addBook(b7);
*/
    }

    private BookRepository bookRepository = new BookRepository();

    private ReservationRepository reservationRepository = new ReservationRepository(bookRepository);

    private String libraryName = "library";

    public String getLibraryName() {
        return libraryName;
    }
    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public BookRepository getBookRepository() {
        return bookRepository;
    }

    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }
}
