package com.library.solution.libraryElements.reservations;

import java.time.LocalDateTime;

class Reservation {

    private LocalDateTime reservationStartDate;
    private LocalDateTime reservationEndDate;

    Reservation() {
        reservationStartDate = LocalDateTime.now();
        reservationEndDate = reservationStartDate.plusDays(30);
    }

    LocalDateTime getReservationStartDate() {
        return reservationStartDate;
    }

    LocalDateTime getReservationEndDate() {
        return reservationEndDate;
    }
}
