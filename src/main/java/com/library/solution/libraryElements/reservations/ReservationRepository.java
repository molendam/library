package com.library.solution.libraryElements.reservations;

import com.library.solution.comunnication.Response;
import com.library.solution.libraryElements.BookRepository;
import com.library.solution.libraryElements.book.Book;
import com.library.solution.libraryElements.book.ISBN;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReservationRepository {

    private Map<ISBN, Reservation> reservations = new HashMap<>();
    private BookRepository bookRepository;

    public ReservationRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    private final String ACTIVE_RESERVARION = "This book has active reservation! Reservation will end: ";
    private final String BOOK_RESERVED = "The book reserved! Reservation will end: ";
    private final String RESERVATION_DELETED = "The reservation has been successfully deleted!";
    private final String RESERVATION_DOES_NOT_EXIST = "This reservation doesn't exist!";

    public Response createReservation(ISBN isbn) {
        String message;
        if (reservations.containsKey(isbn)) {

            message = ACTIVE_RESERVARION;
            message += reservations.get(isbn).getReservationEndDate().toLocalDate();
            return new Response(message, false);
        }
        Reservation res = new Reservation();
        reservations.put(isbn, res);
        message = BOOK_RESERVED + res.getReservationEndDate().toLocalDate();

        return new Response(message, true);
    }

    public List<Book> getAvailableBooks() {
        List<Book> books = new ArrayList<>();
        Map<ISBN, Book> allBooks = bookRepository.getBooksMap();

        for (ISBN isbn : allBooks.keySet()) {
            if (!reservations.containsKey(isbn)) {
                books.add(allBooks.get(isbn));
            }
        }

        /*Map<ISBN, Book> availableBooks = new HashMap<>();
        for (ISBN isbn : allBooks.keySet()) {
            if (!reservations.containsKey(isbn)) {
                availableBooks.put(isbn, allBooks.get(isbn));
            }
        }*/
        return books;
    }

    public Response deleteReservation(ISBN isbn) {
        String message;
        if (!reservations.containsKey(isbn)) {
            message = RESERVATION_DOES_NOT_EXIST;
            return new Response(message, false);
        }
        reservations.remove(isbn);
        message = RESERVATION_DELETED;
        return new Response(message, true);
    }
}
