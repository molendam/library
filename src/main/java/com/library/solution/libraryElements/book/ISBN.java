package com.library.solution.libraryElements.book;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.util.Objects;

public class ISBN implements Serializable {

    private String ISBN;

    static final String ISBN_INCORRECT_LENGTH_MESSAGE = "ISBN length is incorrect (Should be 13 digits)";
    static final String ISBN_NUMBER_FORMAT_EXPECTED_MESSAGE = "ISBN number format is required";
    static final String ISBN_NULL_VALUE_MESSAGE = "This value can't be null";

    @JsonCreator
    public ISBN(@JsonProperty("stringISBN") String ISBN) {
        ISBNvalidator(ISBN);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ISBN)) return false;
        ISBN isbn = (ISBN) o;
        return getStringISBN().equals(isbn.getStringISBN());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStringISBN());
    }

    private void ISBNvalidator(String ISBN) throws IllegalArgumentException {

        if (!ObjectUtils.allNotNull(ISBN)) {
            throw new IllegalArgumentException(ISBN_NULL_VALUE_MESSAGE);
        }

        if (ISBN.length() != 13) {
            throw new IllegalArgumentException(ISBN_INCORRECT_LENGTH_MESSAGE);
        }
        try {
            Long.parseLong(ISBN);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(ISBN_NUMBER_FORMAT_EXPECTED_MESSAGE);
        }

        this.ISBN = ISBN;
    }

    public void setISBN(String ISBN) {
        ISBNvalidator(ISBN);
    }

    public String getStringISBN() {
        return ISBN;
    }


}
