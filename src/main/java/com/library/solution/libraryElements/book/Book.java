package com.library.solution.libraryElements.book;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.time.Year;

public class Book implements Serializable {

    private Year publicationYear;
    private String title;
    private String author;
    private ISBN isbn;

    public Book() {
    }

    static final String PARAMETERS_NULL_VALUE_MESSAGE = "This values can't be null";

    public Book(Year publicationYear, String title, String author, ISBN isbn) {
        if (ObjectUtils.allNotNull(publicationYear, title, author)) {
            this.publicationYear = publicationYear;
            this.title = title;
            this.author = author;
            this.isbn = isbn;
        } else {
            throw new IllegalArgumentException(PARAMETERS_NULL_VALUE_MESSAGE);
        }
    }

    public ISBN getIsbn() {
        return isbn;
    }

    public Year getPublicationYear() {
        return publicationYear;
    }


    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "[Title: " + getTitle() +
                " Author: " + getAuthor() +
                " Publication year: " + getPublicationYear() +
                " ISBN: " + getIsbn().getStringISBN() +
                "]";
    }
}
