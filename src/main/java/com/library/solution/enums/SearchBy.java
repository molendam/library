package com.library.solution.enums;

public enum SearchBy {
    ISBN, TITLE, AUTHOR, ANY_INFORMATION
}
