package com.library.solution.enums;

public enum DataType {
    ASFILES, ASCSV, JSON
}
