package com.library.solution.consoleParts;

import java.util.List;
import java.util.Scanner;

class ConsoleUtilities {

    static void showOptionList(List<String> options) {
        int max = 0;

        for (String o : options) {
            if (o.length() > max) {
                max = o.length();
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < options.size(); i++) {
            int size = max - (options.get(i)).length();
            StringBuilder word = new StringBuilder();
            /*String charr = Arrays.toString(new char[size]) ; //?? jak dodać puste znaki*/
            for (int k = 0; k < size; k++) {
                word.append(' ');
            }
            sb.append("/." + (i + 1) + "./ " + word + options.get(i) + ":\n");
        }
        System.out.println(sb.toString());
    }

    private static int tryGetUserInput(int optionAmount) throws NumberFormatException {
        String userInput = new Scanner(System.in).nextLine();

        int userInputParsed = Integer.parseInt(userInput);
        if (userInputParsed < 1 || userInputParsed > optionAmount) {
            throw new NumberFormatException();
        }
        return userInputParsed;
    }

    //jeśli użytkownik wpisze coś spoza możliwych opcji wyświetlana jest informacja,
    //w przeciwnym wypadku zwraca int po konwersji ze String
    static int getSpecialUserAnswer(int max) {
        int answer = 0;
        try {
            answer = ConsoleUtilities.tryGetUserInput(max);
        } catch (NumberFormatException nfe) {
            System.out.println(Statements.MENU_INCORRECT_ANSWER_ALLERT);
        }
        return answer;
    }

    static void showMainMessage(String message) {
        System.out.println("** " + message + " **");
    }

    //pobiera od użytkownika zwykłą odpowiedź String
    static String getUserAnswer(String prompt) {
        ConsoleUtilities.showMainMessage(prompt);
        return new Scanner(System.in).nextLine();
    }
}
