package com.library.solution.consoleParts;

import java.util.ArrayList;
import java.util.List;

class Statements {

    //GENERAL STATEMENTS
    static final String MENU_PROMPT = "Select option [enter option id]:";
    static final String MENU_INCORRECT_ANSWER_ALLERT = "No way! Try again:";
    //PRIMARY MENU

    static List<String> getPrimaryMenuOptionsList() {
        List<String> primaryMenuOptionsList = new ArrayList<>();
        primaryMenuOptionsList.add("Add new book");
        primaryMenuOptionsList.add("Find book");
        primaryMenuOptionsList.add("Return book");
        primaryMenuOptionsList.add("Show available books");
        primaryMenuOptionsList.add("EXIT");
        return primaryMenuOptionsList;
    }
    //FIND BOOK MENU

    static List<String> getFindMenuOptionsList() {
        ArrayList<String> findBookMenuOptionsList = new ArrayList<>();
        findBookMenuOptionsList.add("Search by any info");
        findBookMenuOptionsList.add("Search by title");
        findBookMenuOptionsList.add("Search by author");
        findBookMenuOptionsList.add("Search by ISBN");
        findBookMenuOptionsList.add("Main Menu");
        findBookMenuOptionsList.add("Exit");
        return findBookMenuOptionsList;
    }

    //BOOK OPERATIONS MENU
    static List<String> getBookOperationMenuOptionList() {
        List<String> bom = new ArrayList<>();
        bom.add("Rent book");
        bom.add("Remove book");
        bom.add("Main menu");
        bom.add("EXIT");
        return bom;
    }

    //PROMPTS
    static final String PROMPT_FIND_BOOK_BY_ANY = "Enter some informations about title/author/year/isbn";
    static final String PROMPT_FIND_BOOK_BY_ISBN = "Enter some informations about isbn";
    static final String PROMPT_FIND_BOOK_BY_AUTHOR = "Enter some informations about author";
    static final String PROMPT_FIND_BOOK_BY_TITLE = "Enter some informations about title";
    static final String PROMPT_ID = "Enter id of book:";
    //ERROR MESSAGES
    static final String ANY_BOOK_IS_AVAILABLE = "Any book is available";
    static final String INCORRECT_ISBN_OR_BOOK_STATUS = "Something is wrong with ISBN or status of book";
    static final String INCORRECT_YEAR_VALUE = "It's not correct value of year!";
    static final String BOOK_NOT_FOUND_ALLERT = "Book not found, try again";
    //LOAD ERRORS
    static final String CAN_NOT_LOAD_BOOKS = "An error occurred while loading the books";
    static final String CAN_NOT_SAVE_BOOKS = "An error occurred while saving books data";
}
