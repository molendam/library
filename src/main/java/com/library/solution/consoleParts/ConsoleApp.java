package com.library.solution.consoleParts;

import com.library.solution.enums.DataType;
import com.library.solution.enums.SearchBy;
import com.library.solution.libraryElements.Library;
import com.library.solution.libraryElements.book.Book;
import com.library.solution.libraryElements.book.BookTitleComparator;
import com.library.solution.libraryElements.book.ISBN;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.Year;
import java.util.List;

public class ConsoleApp {
    private Library library = new Library();

    public void run(String libName) {
        loadData();
        library.setLibraryName(libName);
        String welcomeText = "Welcome to the : \"" + library.getLibraryName() + "\" application!";
        ConsoleUtilities.showMainMessage(welcomeText);
        primaryMenu();
    }

    private void loadData() {
        try {
            library.getBookRepository().loadData(DataType.ASFILES);
        } catch (IOException | ClassNotFoundException e) {
            ConsoleUtilities.showMainMessage(Statements.CAN_NOT_LOAD_BOOKS);
        }
    }

    private void exit() {
        try {
            this.library.getBookRepository().saveData(DataType.ASFILES);
            System.exit(0);
        } catch (IOException e) {
            ConsoleUtilities.showMainMessage(Statements.CAN_NOT_SAVE_BOOKS);
        }
    }


    @SuppressWarnings("InfiniteLoopStatement")
    private void primaryMenu() {
        String answer;
        while (true) {
            List<String> ol = Statements.getPrimaryMenuOptionsList();
            ConsoleUtilities.showOptionList(ol);
            answer = ConsoleUtilities.getUserAnswer(Statements.MENU_PROMPT);
            switch (answer) {
                case "1":
                    addBook();
                    break;
                case "2":
                    findBooks();
                    break;
                case "3":
                    returnBook();
                    break;
                case "4":
                    showAvailableBooks();
                    break;
                case "5":
                    exit();
                default:
                    ConsoleUtilities.showMainMessage(Statements.MENU_INCORRECT_ANSWER_ALLERT);
            }
        }
    }

    private void bookOperationMenu(List<Book> books) {
        ConsoleUtilities.showOptionList(Statements.getBookOperationMenuOptionList());
        String answer = ConsoleUtilities.getUserAnswer(Statements.MENU_PROMPT);
        switch (answer) {
            case "1":
                rentBook(books);
                break;
            case "2":
                removeBook(books);
                break;
            case "3":
                primaryMenu();
                break;
            case "4":
                exit();
            default:
                ConsoleUtilities.showMainMessage(Statements.MENU_INCORRECT_ANSWER_ALLERT);
                bookOperationMenu(books);
        }
    }

    private void removeBook(List<Book> books) {
        ConsoleUtilities.showMainMessage(Statements.PROMPT_ID);
        int userInput = ConsoleUtilities.getSpecialUserAnswer(books.size());
        if (!(userInput == 0)) {
            ISBN isbn =books.get(userInput - 1).getIsbn();
            String message = library.getBookRepository().removeBook(isbn).getMessage();
            ConsoleUtilities.showMainMessage(message);
        }
    }

    private void findBooks() {
        String answer = "";
        while (!answer.equals("5")) {
            ConsoleUtilities.showOptionList(Statements.getFindMenuOptionsList());
            answer = ConsoleUtilities.getUserAnswer(Statements.MENU_PROMPT);
            String searchText = "";
            SearchBy searchBy = null;
            switch (answer) {
                case "1":
                    searchText = ConsoleUtilities.getUserAnswer(Statements.PROMPT_FIND_BOOK_BY_ANY);
                    searchBy = SearchBy.ANY_INFORMATION;
                    break;
                case "2":
                    searchText = ConsoleUtilities.getUserAnswer(Statements.PROMPT_FIND_BOOK_BY_TITLE);
                    searchBy = SearchBy.TITLE;
                    break;
                case "3":
                    searchText = ConsoleUtilities.getUserAnswer(Statements.PROMPT_FIND_BOOK_BY_AUTHOR);
                    searchBy = SearchBy.AUTHOR;
                    break;
                case "4":
                    searchText = ConsoleUtilities.getUserAnswer(Statements.PROMPT_FIND_BOOK_BY_ISBN);
                    searchBy = SearchBy.ISBN;
                    break;
                case "5":
                    primaryMenu();
                    break;
                case "6":
                    exit();
                default:
                    ConsoleUtilities.showMainMessage(Statements.MENU_INCORRECT_ANSWER_ALLERT);
            }
            List<Book> booksFound = library.getBookRepository().findBooks(searchText, searchBy);
            showBookListWithOperations(booksFound);

        }
    }


    private void addBook() {
        try {
            String title = ConsoleUtilities.getUserAnswer("Title of book:");
            String author = ConsoleUtilities.getUserAnswer("Author:");
            Year publicationYear = Year.parse(ConsoleUtilities.getUserAnswer("Publication year:"));
            ISBN isbn = new ISBN(ConsoleUtilities.getUserAnswer("ISBN:"));
            String message = library.getBookRepository().addBook(title, author, isbn, publicationYear).getMessage();
            ConsoleUtilities.showMainMessage(message);
        } catch (IllegalArgumentException bipe) {
            ConsoleUtilities.showMainMessage(bipe.getMessage());
        } catch (DateTimeException dte) {
            ConsoleUtilities.showMainMessage(Statements.INCORRECT_YEAR_VALUE);
        } finally {
            primaryMenu();
        }
    }

    private void rentBook(List<Book> books) {
        ConsoleUtilities.showMainMessage(Statements.PROMPT_ID);
        int userInput = ConsoleUtilities.getSpecialUserAnswer(books.size());
        if (!(userInput == 0)) {
            ISBN isbn = books.get(userInput - 1).getIsbn();
            String message = library.getReservationRepository().createReservation(isbn).getMessage();
            ConsoleUtilities.showMainMessage(message);
        }
    }

    private void showAvailableBooks() {
        List<Book> books = library.getReservationRepository().getAvailableBooks();

        if (books.isEmpty()) {
            ConsoleUtilities.showMainMessage(Statements.ANY_BOOK_IS_AVAILABLE);
        } else {
            showBookListWithOperations(books);
        }
        primaryMenu();
    }

    private void returnBook() {
        String prompt = "Enter ISBN:";
        try {
            ISBN isbn = new ISBN(ConsoleUtilities.getUserAnswer(prompt));
            String message = library.getReservationRepository().deleteReservation(isbn).getMessage();
            ConsoleUtilities.showMainMessage(message);
        } catch (IllegalArgumentException iae) {
            ConsoleUtilities.showMainMessage(Statements.INCORRECT_ISBN_OR_BOOK_STATUS);
        }
    }

    private void showBookListWithOperations(List<Book> books) {
        books.sort(new BookTitleComparator());
        if (books.isEmpty()) {
            ConsoleUtilities.showMainMessage(Statements.BOOK_NOT_FOUND_ALLERT);
        } else {
            int i = 0;
            for (Book b : books
            ) {
                System.out.println(i + 1 + ". " + b.toString());
                i++;
            }
            bookOperationMenu(books);
        }
    }
}
