package com.library.solution.dataManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.library.solution.libraryElements.book.Book;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

class SavingData {
    void saveBooksToCSV(List<Book> books, Path csvFileFullPath) throws IOException {
        String sep = BookDataManager.CSV_SEPARATOR;
        List<String> out = new ArrayList<>();
        for (Book b : books) {
            String s = b.getTitle() + sep +
                    b.getAuthor() + sep +
                    b.getPublicationYear() + sep +
                    b.getIsbn().getStringISBN();
            out.add(s);
        }
        Files.write(csvFileFullPath, out);
    }


    void saveBooksToJSON(List<Book> books, Path bookFileFullPath) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();

        File f = new File(bookFileFullPath.toString());
        objectMapper.writeValue(f, books);
    }

    void serializeBooks(List<Book> books, Path bookFileFullPath) throws IOException {
        ObjectOutputStream ous = new ObjectOutputStream(new FileOutputStream(bookFileFullPath.toString()));
        ous.writeObject(books);
    }
}
