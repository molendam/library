package com.library.solution.dataManager;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.library.solution.libraryElements.book.Book;
import com.library.solution.libraryElements.book.ISBN;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class DataReading {

    List<Book> readObjectsFromCSV(Path path) throws IOException {
        List<Book> books = new ArrayList<>();
        if (!Files.exists(path)) {
            return books;
        }
        BufferedReader br = new BufferedReader((new FileReader(path.toString())));

        String line;
        while ((line = br.readLine()) != null) {
            List<Object> columns;
            columns = Arrays.asList(line.split(BookDataManager.CSV_SEPARATOR));
            String title = (String) columns.get(0);
            String author = (String) columns.get(1);
            Year year = Year.parse((String) columns.get(2));
            ISBN isbn = new ISBN((String) columns.get(3));
            Book book = new Book(year, title, author, isbn);
            books.add(book);
        }
        return books;
    }

    List<Book> deserializeBooks(Path path) throws IOException, ClassNotFoundException {

        if (!Files.exists(path)) {
            return new ArrayList<>();
        }

        File file = new File(path.toString());
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
        return (List<Book>) objectInputStream.readObject();
    }

    List<Book> readObjectsFromJSON(Path path) throws IOException {

        if (!Files.exists(path)) {
            return new ArrayList<>();
        }

        File jsonFile = new File(path.toString());
        ObjectMapper omr = new ObjectMapper().findAndRegisterModules();
        return omr.readValue(jsonFile, new TypeReference<ArrayList<Book>>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        });
    }

}
