package com.library.solution.dataManager;

import com.library.solution.enums.DataType;
import com.library.solution.libraryElements.book.Book;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class BookDataManager {

    private SavingData savingData = new SavingData();
    private DataReading dataReading = new DataReading();
    final static String CSV_SEPARATOR = ",,";

    public List<Book> loadData(DataType dt) throws IOException, ClassNotFoundException {
        List<Book> books = new ArrayList<>();
        if (firstBoot()) {
            createDirectories();
        } else {
            switch (dt) {
                case ASCSV:
                    books = dataReading.readObjectsFromCSV(AppDataPaths.getCSVFileFullPath());
                    break;
                case ASFILES:
                    books = dataReading.deserializeBooks(AppDataPaths.getBookFileFullPath());
                    break;
                case JSON:
                    books = dataReading.readObjectsFromJSON(AppDataPaths.getJsonFileFullPath());
                    break;
            }
        }
        return books;
    }

    public void saveData(DataType st, List<Book> books) throws IOException {

        switch (st) {
            case ASCSV:
                savingData.saveBooksToCSV(books, AppDataPaths.getCSVFileFullPath());
                break;
            case ASFILES:
                savingData.serializeBooks(books, AppDataPaths.getBookFileFullPath());
                break;
            case JSON:
                savingData.saveBooksToJSON(books, AppDataPaths.getJsonFileFullPath());
                break;
            default:
        }
    }

    private boolean firstBoot() {
        return !Files.exists(AppDataPaths.getMainDataDir());
    }

    private void createDirectories() throws IOException {
        Files.createDirectory(AppDataPaths.getMainDataDir());
        Files.createDirectory(AppDataPaths.getCSVDir());
        Files.createDirectory(AppDataPaths.getJsonFileDir());
        Files.createDirectory(AppDataPaths.getBookFileDir());
    }
}


