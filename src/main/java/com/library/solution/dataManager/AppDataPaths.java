package com.library.solution.dataManager;

import java.nio.file.Path;
import java.nio.file.Paths;

final class AppDataPaths {

    private static final String MAIN_DIRECTORY_NAME = "data";
    private static final String CSV_DIRECTORY_NAME = "csv";
    private static final String CSV_FILE_NAME = "books.csv";
    private static final String BOOKS_FILES_DIRECTORY_NAME = "booksFiles";
    private static final String BOOKS_FILES_FILE_NAME = ".bks";
    private static final String JSON_DIRECTORY_NAME = "json";
    private static final String JSON_FILE_NAME = "books.json";

    static Path getMainDataDir() {
        return Paths.get(MAIN_DIRECTORY_NAME);
    }

    static Path getCSVDir() {
        return Paths.get(MAIN_DIRECTORY_NAME + "\\" + CSV_DIRECTORY_NAME + "\\");
    }

    static Path getCSVFileFullPath() {
        return Paths.get(getCSVDir() + "\\" + CSV_FILE_NAME);
    }

    static Path getBookFileDir() {
        return Paths.get(getMainDataDir() + "\\" + BOOKS_FILES_DIRECTORY_NAME);
    }

    static Path getBookFileFullPath() {
        return Paths.get(getBookFileDir() + "\\" + BOOKS_FILES_FILE_NAME);
    }

    public static Path getJsonFileDir() {
        return Paths.get(getMainDataDir() + "\\" + JSON_DIRECTORY_NAME);
    }

    public static Path getJsonFileFullPath() {
        return Paths.get(getJsonFileDir() + "\\" + JSON_FILE_NAME);
    }


}
