package com.library.solution.libraryElements.book;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Year;

public class BookTest {

    @Test
    void creatingBookObjectWithNullParametersShouldTrowIllegalArgumentException()
    {
        IllegalArgumentException iae1 = Assertions.assertThrows(IllegalArgumentException.class,
                ()-> new Book(null,null,null,null));
        IllegalArgumentException iae2 = Assertions.assertThrows(IllegalArgumentException.class,
                ()-> new Book(Year.of(1234),null,null,null));
        IllegalArgumentException iae3 = Assertions.assertThrows(IllegalArgumentException.class,
                ()-> new Book(null,null,"test",null));
        IllegalArgumentException iae4 = Assertions.assertThrows(IllegalArgumentException.class,
                ()-> new Book(Year.of(2005),null,"test",null));
        Assertions.assertEquals(Book.PARAMETERS_NULL_VALUE_MESSAGE,iae1.getMessage());

    }
   /* @Test

    void creatingBookObjectWithCorrectParametersShouldNotTrowAnyException()
    {
        Assertions.assertDoesNotThrow(
                ()-> new Book(Year.of(2005),"Title","Author",new ISBN("1111111111111")));
    }*/

}
