package com.library.solution.libraryElements.book;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ISBNtest {
   /* @Test
    void ISBNConstructWithCorrectValue() {
         Assertions.assertDoesNotThrow( () -> {
            new ISBN("1231231233123");
        });
    }*/
    @Test
    void ISBNConstructWithNullValue() {
        IllegalArgumentException iae = Assertions.assertThrows(IllegalArgumentException.class,
                () -> {new ISBN(null); });
        Assertions.assertEquals(ISBN.ISBN_NULL_VALUE_MESSAGE,iae.getMessage());
    }
    @Test
    void ISBNConstructWithEmptyValue() {
        IllegalArgumentException iae= Assertions.assertThrows(IllegalArgumentException.class,
                () -> { new ISBN("");});
        Assertions.assertEquals(ISBN.ISBN_INCORRECT_LENGTH_MESSAGE,iae.getMessage());

    }
    @Test
    void ISBNConstructWithNotNumberValue() {
        IllegalArgumentException iae= Assertions.assertThrows(IllegalArgumentException.class,
                () -> { new ISBN("testtesttestt");});
        Assertions.assertEquals(ISBN.ISBN_NUMBER_FORMAT_EXPECTED_MESSAGE,iae.getMessage());

    }
}
